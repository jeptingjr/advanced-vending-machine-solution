## Backstory

For, what is in my opinion, the only reputable coding bootcamp out there, Tech Elevator, I wrote a more "advanced" solution to a vending machine project we had our students create. In short they were supposed to use their knowledge of Collections, Interfaces, File IO, and program control flow to implement a console application that behaved as a vending machine. The students I worked with were extremely passionate and curious about coding, so I decided to create an "advanced" solution (really it is overengineered) to pique their interest. 

## Project Breakdown

This solution was designed around the idea of keeping our vending machine implementation easy to modify. By doing so the
logic of our menus are compartmentalized such that any implementation issues can be tracked back to a single method call.
This approach also resulted in a framework for menus, which is just a design suggestion future developers can leverage to
get work done faster. Adding new menu options, an entirely new menu, or changing functionality is straightforward with
this design despite its complexity. 

## Application flow
1. Application Startup
2. VendingMachine.java reads CSV file and initializes an inventory HashMap
3. MainMenu.java is selected as the starting menu (the currentMenu in code)
4. The main application loop begins
5. currentMenu is ran
6. The user is prompted to make a selection from currentMenu's menu options
7. The menu option selected has its method code ran
8. The menu option's method returns an EnhancedMenu object
9. The EnhancedMenu object returned from the method call becomes the new currentMenu
10. Repeat steps 5 - 9 until the vending machine is turned off
