package com.techelevator.exception;

/* A custom exception to make the code a little more readable */
public class BadInputException extends Exception {
    public BadInputException(String message) {
        super(message);
    }
}