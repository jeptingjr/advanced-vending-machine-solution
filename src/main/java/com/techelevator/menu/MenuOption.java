package com.techelevator.menu;

import java.util.concurrent.Callable;

/* This class represents a generic menu option our user can choose */
public class MenuOption {
    private String optionText; // The text of the option, ie: Feed Money
    private Callable<EnhancedMenu> action; // The function that will be called when an option is selected @ EnhancedMenu line 37
    private boolean isHidden; // Flag to prevent the menu option from being printed out

    public MenuOption(String optionText, Callable<EnhancedMenu> action) {
        this.optionText = optionText;
        this.action = action;
        this.isHidden = false;
    }

    public MenuOption(String optionText, Callable<EnhancedMenu> action, boolean isHidden) {
        this.optionText = optionText;
        this.action = action;
        this.isHidden = isHidden;
    }

    public EnhancedMenu runAction() throws Exception {
        return action.call(); // Runs the function bound to this menu option
    }

    public boolean isHidden() {
        return isHidden;
    }

    public String getOptionText() {
        return optionText;
    }
}