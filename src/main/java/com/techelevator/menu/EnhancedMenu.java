package com.techelevator.menu;

import com.techelevator.exception.BadInputException;
import com.techelevator.vending.VendingMachine;
import java.util.ArrayList;
import java.util.List;
public abstract class EnhancedMenu {
	private List<MenuOption> menuOptions = new ArrayList<>();
	private static EnhancedMenu selfReference;
	public EnhancedMenu() {
		this.selfReference = this;
		initialize(); // The subclasses will implement this function!
	}

	/* These protected methods need to be inherited by our subclasses, BUT! we don't want to give any other classes
	 * the ability to run any of these methods. This is why protected is used, outside of the menu package a menu instance
	 * can only call run() */

	// This method stub forces any subclasses to declare it's own custom menu options
	protected abstract void initialize();

	// This stub forces subclasses to also implement a custom welcome message
	protected abstract void displayMenuWelcome();

	protected static EnhancedMenu getSelfReference() {
		return selfReference; // Allows any subclasses of EnhancedMenu to reference itself in a static context
	}

	protected void setMenuOptions (List<MenuOption> menuOptions) {
		this.menuOptions = menuOptions;
	}
	public EnhancedMenu run() {
		try {
			display();
			int userInput = getUserMenuChoice();
			// Calls the function associated to the menu option selected
			return menuOptions.get(userInput - 1).runAction();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return this;
		}
	}

	private void display() {
		/* We can reference unimplemented method stubs here because once an EnhancedMenu is implemented and initialized
		 * the method stub becomes a real method, check out MainMenu and TransactionMenu to see their implementations */
		displayMenuWelcome();
		if(menuOptions.size() > 0) { // menuOptions should be initialized by the subclass' initialize() function
			for (int menuOptionNumber = 1; menuOptionNumber <= menuOptions.size(); menuOptionNumber++) {
				MenuOption currentMenuOption = menuOptions.get(menuOptionNumber - 1);
				if(!currentMenuOption.isHidden()) { // Supports hidden menu options
					System.out.println(menuOptionNumber + ") " + menuOptions.get(menuOptionNumber - 1).getOptionText());
				}
			}
		}
	}

	private int getUserMenuChoice() throws BadInputException {
		String rawUserInput = VendingMachine.INSTANCE.getUserInput();
		if(rawUserInput.matches("[0-9]+")) { // Checks to see if the user input a number
			int possiblyValidInput = Integer.parseInt(rawUserInput);
			if(possiblyValidInput > 0 && possiblyValidInput <= menuOptions.size()) {
				return possiblyValidInput;
			} else {
				throw new BadInputException("The menu option number: " + possiblyValidInput + " Does not exist!"
				                            + System.lineSeparator()
				                            + "Please enter a numerical value from 1 to " + menuOptions.size());
			}
		} else {
			throw new BadInputException("Your input of " + rawUserInput + " is not a valid option!"
			                            + System.lineSeparator()
					                    + "Please enter a numerical value from 1 to " + menuOptions.size());
		}
	}
}