package com.techelevator.menu;

import com.techelevator.vending.VendingItem;
import com.techelevator.vending.VendingMachine;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class TransactionMenu extends EnhancedMenu {

    /* Check out MainMenu.java first as it has less complicated code and more comments! */
    @Override
    protected void initialize() {
        List<MenuOption> menuOptions = new ArrayList<>();
        menuOptions.add(new MenuOption("Feed Money", TransactionMenu::feedMoney));
        menuOptions.add(new MenuOption("Select Product", TransactionMenu::selectProduct));
        menuOptions.add(new MenuOption("Finish Transaction", TransactionMenu::finishTransaction));

        super.setMenuOptions(menuOptions);
    }

    @Override
    protected void displayMenuWelcome() {
        System.out.println("Get your money ready in hand! Let's take your tongue to flavor land!");
        System.out.println();
        System.out.println("What will you decide?");
        System.out.println();
        System.out.println("[CURRENT FUNDS: $" + VendingMachine.INSTANCE.getUserTransactionBalance() + "]");
        System.out.println();
    }

    private static EnhancedMenu feedMoney() {
        boolean userInputIsBad;
        String rawUserInput;
        do {
            System.out.println("Enter the amount of money you'd like to add"
                    + System.lineSeparator()
                    + "Please use whole dollar amounts! Or else I'll be sad :(");
            rawUserInput = VendingMachine.INSTANCE.getUserInput();
            // This checks to see if the user input a whole dollar amount (5, 6.00, 20.00, 40, and so on)
            userInputIsBad = !rawUserInput.matches("^(([1-9]\\d{0,2}(,\\d{3})*)|(([1-9]\\d*)?\\d))(\\.00)?");
            if (userInputIsBad) {
                System.out.println("Hey! I don't have time for fools! Please just follow the rules!");
            }
        } while (userInputIsBad);
        VendingMachine.INSTANCE.addToTransactionBalance(new BigDecimal(rawUserInput));
        return getSelfReference();
    }

    private static EnhancedMenu selectProduct() {
        VendingMachine.INSTANCE.displayVendingInventory();
        boolean productCodeIsMissing = false;
        boolean itemOutOfStock = false;
        String rawUserInput;
        do {
            System.out.println("One step closer to a delicious treat!"
                    + System.lineSeparator()
                    + "Enter the product code of what you'd like to eat!"
                    + System.lineSeparator()
                    + "You have $" + VendingMachine.INSTANCE.getUserTransactionBalance() + " to spend friend!");

            rawUserInput = VendingMachine.INSTANCE.getUserInput().toUpperCase();
            productCodeIsMissing = !VendingMachine.INSTANCE.getVendingInventory().containsKey(rawUserInput);
            if (productCodeIsMissing) {
                System.out.println("Hey! " + rawUserInput + " isn't a valid code! Your time to snacking has been slowed!");
            } else {
                VendingItem vendingItem = VendingMachine.INSTANCE.getVendingInventory().get(rawUserInput);
                itemOutOfStock = vendingItem.getCurrentItemCount() == 0;
                if(itemOutOfStock) {
                    System.out.println("I know this may be a shock, but the item you requested is out of stock!");
                } else {
                    BigDecimal userTransactionBalance = VendingMachine.INSTANCE.getUserTransactionBalance();
                    boolean insufficientFunds = vendingItem.getProductPrice().compareTo(userTransactionBalance) == 1;
                    if(insufficientFunds) {
                        BigDecimal difference = vendingItem.getProductPrice().subtract(userTransactionBalance);
                        System.out.printf("You are $%.2f short! You need to add more money!%nThe %s is worth it, it will make your day sunny!%n",
                                difference, vendingItem.getProductName());
                        // If the user doesn't have enough money, break the do-while loop and go back to the TransactionMenu
                        return getSelfReference();
                    }
                }
            }
        } while (productCodeIsMissing || itemOutOfStock);

        VendingMachine.INSTANCE.vend(rawUserInput);

        return getSelfReference();
    }

    private static EnhancedMenu finishTransaction() {
        System.out.println("Thank you for spending your money!");
        System.out.println("I hope your choice was as sweet as ~honey~");
        System.out.println();
        if (VendingMachine.INSTANCE.getUserTransactionBalance().compareTo(new BigDecimal(0)) == 1) {
            System.out.println("Wait, I have something to give you!");
            System.out.println("You get... " + VendingMachine.INSTANCE.getChange());
            System.out.println("Okay! Now we are through!");
        }
        System.out.println();
        return new MainMenu(); // "Travel" back to the main menu
    }
}