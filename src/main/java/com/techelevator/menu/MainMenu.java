package com.techelevator.menu;

import com.techelevator.vending.VendingItem;
import com.techelevator.vending.VendingMachine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainMenu extends EnhancedMenu {

    @Override
    protected void initialize() {
        /* The initialize function implementation shows the general layout of our menu and binds functions to each menu
         * option. This approach allows us to be agnostic about which menu option belongs to which number, while
         * facilitating the addition or removal of menu options*/
        List<MenuOption> menuOptions = new ArrayList<>();
        // MainMenu::displayVendingMachineItems is a static method reference, this allows us to treat methods like objects!
        menuOptions.add(new MenuOption("Display Vending Machine Items", MainMenu::displayVendingMachineItems));
        menuOptions.add(new MenuOption("Purchase", MainMenu::purchase));
        menuOptions.add(new MenuOption("Exit", MainMenu::exit));
        menuOptions.add(new MenuOption("SalesReport", MainMenu::salesReport, true));
        super.setMenuOptions(menuOptions);
    }

    @Override
    protected void displayMenuWelcome() { // This function is used in the super class EnhancedMenu
        System.out.println("In a bustling corner, sleek and gleaming bright,\n" +
                "Stands a marvel of convenience, a true delight...\n" +
                "Oh! Behold the vending machine! a wondrous sight!\n" +
                "Dispensing treats and beverages with all its might!");
        System.out.println();
        System.out.println("What will you decide?");
        System.out.println();
    }

    private static EnhancedMenu displayVendingMachineItems() {
        VendingMachine.INSTANCE.displayVendingInventory();
        /* The main application loop needs to know "where" to go after each menu option is selected.
         * Here we return the self reference to "stay" on the main menu, if we returned a different EnhancedMenu
         * implementation, after this function is called we would "travel" to the different menu */
        return getSelfReference();
    }

    private static EnhancedMenu purchase() {
        return new TransactionMenu(); // Here we "travel" to the transaction menu
    }

    private static EnhancedMenu exit() {
        VendingMachine.INSTANCE.turnOffVendingMachine();
        System.out.println();
        System.out.println("Goodbye! I had fun! Thanks for giving this ol' machine a run!");
        return getSelfReference();
    }

    private static EnhancedMenu salesReport() {
        System.out.printf("%-12s %-20s %-16s %s%n", "Product Code", "Product Name", "Amount Sold", "Product Sales");
        Map<String, VendingItem> currentInventory = VendingMachine.INSTANCE.getVendingInventory();
        BigDecimal totalVendingSales = new BigDecimal(0);
        for(String productCode : currentInventory.keySet()) {
            VendingItem currentProduct = currentInventory.get(productCode);
            int amountSold = 5 - currentProduct.getCurrentItemCount();
            BigDecimal totalSales = currentProduct.getProductPrice().multiply(new BigDecimal(amountSold));
            System.out.printf("%-12s %-20s %6s %18s%n", productCode, currentProduct.getProductName(), amountSold, totalSales);
            totalVendingSales = totalVendingSales.add(totalSales);
        }
        System.out.println();
        System.out.println("** TOTAL VENDING SALES -> $" + totalVendingSales + " **");
        System.out.println();
        return getSelfReference();
    }
}
