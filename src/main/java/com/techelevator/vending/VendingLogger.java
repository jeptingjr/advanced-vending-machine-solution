package com.techelevator.vending;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VendingLogger {
    private File outputFile;
    private FileWriter fileWriter;

    public VendingLogger(File file) {
        this.outputFile = file;
    }

    public void logFeedMoney(BigDecimal moneyDelta, BigDecimal newBalance) {
        smartLog("FEED MONEY", "+", moneyDelta, newBalance);
    }

    public void logProductSale(String productCode, String productName, BigDecimal moneyDelta, BigDecimal newBalance) {
        String itemDetails = "[" + productCode + " " + productName + "]";
        smartLog("PRODUCT SALE " + itemDetails, "-", moneyDelta, newBalance);
    }

    public void logChangeReturn(BigDecimal moneyDelta, BigDecimal newBalance) {
        smartLog("CHANGE RETURN", "", moneyDelta, newBalance);
    }

    public void saveLog() {
        try {
            this.fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void smartLog(String contextMessage, String plusOrMinus, BigDecimal moneyDelta, BigDecimal newBalance) {
        lazyLoadFileWriter();
        try {
            fileWriter.write(getTimeStamp() + " >> "
                    + contextMessage
                    + " (" + plusOrMinus + "$" + moneyDelta + " | NEW BALANCE: $" + newBalance + ")"
                    + System.lineSeparator());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void lazyLoadFileWriter() {
        try {
            if (this.fileWriter == null) {
                this.fileWriter = new FileWriter(this.outputFile, true);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private String getTimeStamp() {
        DateFormat timeStampFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
        return timeStampFormat.format(new Date()).toString();
    }
}