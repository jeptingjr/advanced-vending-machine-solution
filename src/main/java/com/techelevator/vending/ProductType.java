package com.techelevator.vending;

/* Just a simple constants class to make our code more readable */
public class ProductType {
    public static final String CANDY = "Candy";
    public static final String CHIP = "Chip";
    public static final String DRINK = "Drink";
    public static final String GUM = "Gum";
}