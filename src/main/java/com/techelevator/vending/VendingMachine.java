package com.techelevator.vending;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/* This class uses what is called a singleton design pattern, in short this design pattern helps us manage global state
 * by guaranteeing that we will always access the same object. Since this class represents the VendingMachine, the methods
 * implemented are only concerned about the bare bones functionality of a vending machine. By separating the core
 * functionality of our application into its own class, we can make changes elsewhere in our application without worrying
 * about breaking this implementation.
 */
public class VendingMachine {
    /* This variable is what guarantees that we are referencing the same object throughout our application */
    public static final VendingMachine INSTANCE = new VendingMachine();

    // TreeMaps order the keys for us, so when our inventory is printed to the console the product codes are in order!
    private final Map<String, VendingItem> vendingInventory = new TreeMap<>();
    private boolean isRunning = true;
    private BigDecimal userTransactionBalance = new BigDecimal(0);
    private Scanner inputSource = new Scanner(System.in);
    private VendingLogger vendingLogger;

    /* A private constructor means only this class can instantiate itself, which further guarantees no other VendingMachine
     * instance can be made to replace the one our application is referencing */
    private VendingMachine() {
        this.vendingLogger = new VendingLogger(new File("vending_log.txt"));
    }

    // Reads in the CSV file and converts its contents to a hash map
    public void initialize() {
        File vendingMachineCSV = new File("vendingmachine.csv");
        try (Scanner fileScanner = new Scanner(vendingMachineCSV.getAbsoluteFile())) {
            while (fileScanner.hasNextLine()) {
                String rawFileLine = fileScanner.nextLine();
                String[] vendingItemLine = rawFileLine.split("\\|");
                String productCode = vendingItemLine[0];
                String productName = vendingItemLine[1];
                String productType = vendingItemLine[3];
                BigDecimal productPrice = new BigDecimal(vendingItemLine[2]);
                vendingInventory.put(productCode, new VendingItem(productName, productType, productPrice));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public Map<String, VendingItem> getVendingInventory() {
        return vendingInventory;
    }

    public void displayVendingInventory() {
        System.out.printf("%-4s %-20s %-11s %s", "Slot", "Item", "Price", "Quantity");
        System.out.println();
        Map<String, VendingItem> vendingMachineInventoryMap = getVendingInventory();
        for (String productCode : vendingMachineInventoryMap.keySet()) {
            VendingItem vendingItem = vendingMachineInventoryMap.get(productCode);
            if (vendingItem.getCurrentItemCount() > 0) {
                System.out.printf("%-4s %-20s $%-10.2f %4s%n",
                        productCode, vendingItem.getProductName(),
                        vendingItem.getProductPrice(), vendingItem.getCurrentItemCount());
            } else {
                // When a vending machine is sold out of an item you can still see the slot, although it is empty!
                System.out.printf("%-4s %-20s $%-10s %s%n",
                        productCode, "------", "----", "SOLD OUT");
            }
        }
        System.out.println();
    }

  /* The vending machine class doesn't do any input validations, this is because our menu classes are better suited to
   * validate user input and prompt them to retry. It is a good design practice to have similar functionalities in the
   * same place, although the VendingMachine class could do some validations on it's own (for example here we could
   * check if the productCode exists in our map), we don't want to have to check multiple places for validation logic.
   * It is much easier just to know that only the menus do validation, so if we have a bug, we know where exactly to
   * start searching. */
    public void vend(String productCode) {
        VendingItem vendingItem = vendingInventory.get(productCode);
        vendingItem.reduceItemCount();
        String previousBalance = userTransactionBalance.toString();
        reduceTransactionBalance(vendingItem.getProductPrice());
        System.out.println("Your " + vendingItem.getProductName() + " cost $" + vendingItem.getProductPrice());
        System.out.println("Your balance was: $" + previousBalance + " and now it is: $" + userTransactionBalance);
        System.out.println("Hey don't have a heart attack! Please wait while I vend your snack!!");
        dramaticPause(); // Vending machines aren't instant in real life, so here is a bit of "immersion"
        System.out.println();
        System.out.println("VENDING COMPLETE!");
        vendingLogger.logProductSale(productCode, vendingItem.getProductName(), vendingItem.getProductPrice(), userTransactionBalance);
        /* The "product sounds" have no bearing on our logic, so they can exist outside of our product class, and can be
         * used when needed */
        switch (vendingItem.getProductType()) {
            case ProductType.CANDY:
                System.out.println("Munch Munch, Yum!");
                break;
            case ProductType.CHIP:
                System.out.println("Crunch Crunch, Yum!");
                break;
            case ProductType.DRINK:
                System.out.println("Glug Glug, Yum!");
                break;
            case ProductType.GUM:
                System.out.println("Chew Chew, Yum!");
                break;
            default:
                System.out.println("Beep Boop Beep Boop");
        }
        System.out.println();
    }

    public String getChange() {
        int convertedBalance = userTransactionBalance.multiply(new BigDecimal(100)).intValue();
        int quarters = convertedBalance / 25;
        int dimes = (convertedBalance - 25 * quarters) / 10;
        int nickles = (convertedBalance - (25 * quarters) - (10 * dimes)) / 5;

        // The rest of the code here provides a grammatically correct output string
        Map<String, Integer> changeMap = new HashMap<>();
        if (quarters > 0) {
            changeMap.put("Quarter", quarters);
        }
        if (dimes > 0) {
            changeMap.put("Dime", dimes);
        }
        if (nickles > 0) {
            changeMap.put("Nickle", nickles);
        }

        StringBuilder changeString = new StringBuilder();
        int mapIndex = 0;
        for (Map.Entry<String, Integer> mapEntry : changeMap.entrySet()) {
            int coinAmount = mapEntry.getValue();
            if (coinAmount > 0) {
                changeString.append(coinAmount + " " + mapEntry.getKey());
                if (coinAmount > 1) {
                    changeString.append("s"); // Make the coin's name plural if we need to
                }
                changeString.append(" ");
            }
            mapIndex++;
            if (mapIndex + 1 == changeMap.size()) {
                changeString.append("and "); // Append 'and' if we need to
            }
        }
        vendingLogger.logChangeReturn(userTransactionBalance, new BigDecimal(0));
        userTransactionBalance = new BigDecimal(0);
        return changeString.toString();
    }

    public void turnOffVendingMachine() {
        isRunning = false;
        vendingLogger.saveLog();
    }

    public boolean isRunning() {
        return isRunning;
    }

    public String getUserInput() {
        return inputSource.nextLine();
    }

    public BigDecimal getUserTransactionBalance() {
        return userTransactionBalance;
    }

    public void addToTransactionBalance(BigDecimal additionalMoney) {
        this.userTransactionBalance = this.userTransactionBalance.add(additionalMoney);
        vendingLogger.logFeedMoney(additionalMoney, userTransactionBalance);
    }

    public void reduceTransactionBalance(BigDecimal spentMoney) {
        this.userTransactionBalance = this.userTransactionBalance.subtract(spentMoney);
    }

    private void dramaticPause() {
        try {
            for (int repeatIndex = 0; repeatIndex < 15; repeatIndex++) {
                System.out.print("."); // Print something to the console so our app doesn't appear frozen
                Thread.sleep(300); // "Pause" code execution for 300 milliseconds
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
    }
}