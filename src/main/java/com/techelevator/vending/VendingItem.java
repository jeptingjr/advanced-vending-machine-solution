package com.techelevator.vending;

import java.math.BigDecimal;

public class VendingItem {

    private String productName;
    private BigDecimal productPrice;
    private String productType;

    public String getProductName() {
        return productName;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public String getProductType() {
        return productType;
    }

    public int getCurrentItemCount() {
        return currentItemCount;
    }

    public void reduceItemCount() {
        currentItemCount -= 1;
    }

    private int currentItemCount;

    public VendingItem(String productName, String productType, BigDecimal productPrice) {
        this.productName = productName;
        this.productType = productType;
        this.productPrice = productPrice;
        this.currentItemCount = 5;
    }
}