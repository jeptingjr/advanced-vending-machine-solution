package com.techelevator;

import com.techelevator.vending.VendingMachine;
import com.techelevator.menu.EnhancedMenu;
import com.techelevator.menu.MainMenu;

public class VendingMachineCLI {
    public static void main(String[] args) {
        // This function call takes the data from vendingmachine.csv and initializes a private map in the VendingMachine class
        VendingMachine.INSTANCE.initialize();
        EnhancedMenu currentMenu = new MainMenu(); // Set the start point of our application to the main menu
        while (VendingMachine.INSTANCE.isRunning()) {
            /* When each menu option is ran it returns a reference to the "next" menu */
            currentMenu = currentMenu.run();
        }
    }
}
